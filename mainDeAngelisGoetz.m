%######################################################################
% Authors: Chris DeAngelis, Tristan Goetz
% Function: mainDeAngelisGoetz
% 
% This function performs pills segmentaiton based on Hough Circles, region
% growing and color segmentation to identify areas that are likely
% pills.
%
% fn: A string of the input image that is to be segmented
% output: The color segmented image
%######################################################################

function output = mainDeAngelisGoetz(fn)

    % Reads a file into memory
    pills1 = imread(fn);

    colorData = [];

    % Converts the image to Grayscale
    pills1IM = im2double(pills1);
    grayPill = rgb2gray(pills1);
    hsvIM = rgb2hsv(pills1IM);
    [y,x] = size(pills1(:,:,1));

    % Converts the original RGB image into CIE LAB color space
    colorTransform = makecform('srgb2lab');
    lab = applycform(pills1, colorTransform);
    lab = im2double(lab);
    
    %Prepares the output image
    outIM = zeros(y,x);

    % Apply the Canny Edge Detection Algorithm
    [edges, t1] = edge(grayPill, 'canny');
    edges = double(edges);

    % Add the edge image to the b* channel of the LAB color image
    EdgeIM = lab(:,:,3)+(edges);

    % Use Hough Transform to find 'bright' pills against a dark background.
    [centers, radii, metric] = imfindcircles(pills1, [14, 30], 'ObjectPolarity', 'bright', 'Sensitivity', 0.9, 'Edge', 0.001);

    figure, imshow(EdgeIM), title('Edges and Circles found with Hough Edges');
    hold on;
    
    % For each returned circle origin, apply region growing algorithm
    for r=1:length(centers),
        %disp(r);
        x = floor(centers(r,1)); y = floor(centers(r,2));
        out = regiongrowing(im2double(EdgeIM),y,x,0.015);
        
        % Get the connected component properties of the returned image
        Stats = regionprops(out, 'Area', 'ConvexHull');
        
        % Check to see if background is captured by the pill
        % by using a size threshold
        if Stats.Area <=7000,
            plot(x,y,'r.','MarkerSize',5);
            outIM = outIM + out(:,:,1);
            pxl = lab(round(y), round(x),:);
            colorData = [colorData; pxl];   
        end   
    end

    % Use Hough Transform to find 'dark' pills against a bright background.
    [centers, radii, metric] = imfindcircles(pills1, [14, 30], 'ObjectPolarity', 'dark', 'Sensitivity', 0.9, 'Edge', 0.001);

    for r=1:length(centers),
        %disp(r);
        x = floor(centers(r,1)); y = floor(centers(r,2));
        out = regiongrowing(im2double(EdgeIM),y,x,0.015);
        % Get the connected component properties of the returned image
        Stats = regionprops(out, 'Area', 'ConvexHull');
        plot(x,y,'r.','MarkerSize',5);
        % Check to see if background is captured by the pill
        % by using a size threshold
        if Stats.Area <=7000,
            outIM = outIM + out(:,:,1);
            pxl = lab(round(y), round(x),:);
            colorData = [colorData; pxl];  
        end
    end

    hold off;

    % Get the color data of the center points
    data = zeros(length(colorData(:,1,1)), 3);
    for i=1:length(data),
        data(i,1) = colorData(i,1,1);
        data(i,2) = colorData(i,1,2);
        data(i,3) = colorData(i,1,3);
    end

    [a,b] = size(pills1(:,:,1));
    MinDMap = pills1IM(:,:,1);
    
    %Applies color segmentation to attempt to capture "missed" pills
    % using Delta E* Lab color metric
    for x =1:a
        for y = 1:b
            P = [lab(x,y,1),lab(x,y,2),lab(x,y,3)];
            minDist = Inf;
            % Iterates over each data point from the detected centers
            for d=1:length(data),
                curDist = sqrt( (P(1)-data(d,1))^2+(P(2)-data(d,2))^2+(P(3)-data(d,3))^2 );
                if curDist < minDist,
                    minDist = curDist;
                end
            end
            MinDMap(x,y) = minDist;
        end
    end

    thresh = MinDMap < 0.005;

    % Subtracts the edges from the output image to add the edges
    % into the final decisions
    outIM = outIM - edges;
    outIM = outIM > 0;

    % Fills holes in the output image and cleans single pixels
    outIM= imfill(outIM,'holes');
    outIM = bwmorph(outIM, 'clean');

    figure, imshow(outIM), title('Region Growing Results');

    % Fills holes in the color threshold image and cleans single pixels
    thresh= imfill(thresh,'holes');
    thresh= bwmorph(thresh,'clean');

    figure, imshow(thresh), title('Color Thresholding Results');

    finalOut = outIM+thresh;
    finalOut = finalOut > 0;
    finalOut= imfill(finalOut,'holes');

    % Applies erosion to remove very small targets
    se = strel('disk',5);        
    finalOut = imerode(finalOut,se); 

    % Finds all the connected components in the output mask
    foundPills = regionprops(finalOut, 'ConvexArea', 'Image', 'BoundingBox', 'Area');
    CC = bwconncomp(finalOut);

    % For each connected component, measure the difference between
    % the Area and the ConvexArea.  If they are different, then erode the
    % component until further connected component analysis of the subimage
    % shows a different number of components
    for i=1:length(foundPills),
        area = foundPills(i).Area;
        convexArea = foundPills(i).ConvexArea;
        if convexArea >=1.1* area && area > 1000,
            disp('Splitting Pills');
            pts = foundPills(i).BoundingBox;
            x1 = floor(pts(1)); x2 = floor(pts(1))+pts(3);
            y1 = floor(pts(2)); y2 = floor(pts(2))+pts(4);
            splitImage = foundPills(i).Image;
            ccOld = bwconncomp(splitImage);
            se = strel('disk',5); 
            while ccOld.NumObjects == 1,       
                splitImage = imerode(splitImage,se);
                ccOld = bwconncomp(splitImage);
            end
            % Inserts the subimage back into the mask image
            finalOut(y1:y2-1,x1:x2-1) = splitImage;
        end
    end

    pillEdges = bwmorph(finalOut, 'clean');
    pillEdges = bwmorph(pillEdges, 'remove');
    pillEdges = bwmorph(pillEdges, 'dilate');

    output = pills1IM;
    output(:,:,1) = pills1IM(:,:,1)-(pillEdges);
    output(:,:,2) = pills1IM(:,:,2)-(pillEdges);
    output(:,:,3) = pills1IM(:,:,3)-(pillEdges);

    figure, imshow(output, []), title('Final Segmentation');

    % Get the total number of pills and display it to the user.
    CC = bwconncomp(finalOut);

    disp(strcat('Number of pills: ',num2str(CC.NumObjects)));
end

%######################################################################
% Author: D. Kroon
% Function: Region Growing
% Source: http://www.mathworks.com/matlabcentral/fileexchange/19084-region-growing
%######################################################################
function J=regiongrowing(I,x,y,reg_maxdist)
% This function performs "region growing" in an image from a specified
% seedpoint (x,y)
%
% J = regiongrowing(I,x,y,t) 
% 
% I : input image 
% J : logical output image of region
% x,y : the position of the seedpoint (if not given uses function getpts)
% t : maximum intensity distance (defaults to 0.2)
%
% The region is iteratively grown by comparing all unallocated neighbouring pixels to the region. 
% The difference between a pixel's intensity value and the region's mean, 
% is used as a measure of similarity. The pixel with the smallest difference 
% measured this way is allocated to the respective region. 
% This process stops when the intensity difference between region mean and
% new pixel become larger than a certain treshold (t)
%
% Example:
%
% I = im2double(imread('medtest.png'));
% x=198; y=359;
% J = regiongrowing(I,x,y,0.2); 
% figure, imshow(I+J);
%
% Author: D. Kroon, University of Twente

if(exist('reg_maxdist','var')==0), reg_maxdist=0.2; end
if(exist('y','var')==0), figure, imshow(I,[]); [y,x]=getpts; y=round(y(1)); x=round(x(1)); end

J = zeros(size(I)); % Output 
Isizes = size(I); % Dimensions of input image

reg_mean = I(x,y); % The mean of the segmented region
reg_size = 1; % Number of pixels in region

% Free memory to store neighbours of the (segmented) region
neg_free = 10000; neg_pos=0;
neg_list = zeros(neg_free,3); 

pixdist=0; % Distance of the region newest pixel to the regio mean

% Neighbor locations (footprint)
neigb=[-1 0; 1 0; 0 -1;0 1];

% Start regiogrowing until distance between regio and posible new pixels become
% higher than a certain treshold
while(pixdist<reg_maxdist&&reg_size<numel(I))

    % Add new neighbors pixels
    for j=1:4,
        % Calculate the neighbour coordinate
        xn = x +neigb(j,1); yn = y +neigb(j,2);
        
        % Check if neighbour is inside or outside the image
        ins=(xn>=1)&&(yn>=1)&&(xn<=Isizes(1))&&(yn<=Isizes(2));
        
        % Add neighbor if inside and not already part of the segmented area
        if(ins&&(J(xn,yn)==0)) 
                neg_pos = neg_pos+1;
                neg_list(neg_pos,:) = [xn yn I(xn,yn)]; J(xn,yn)=1;
        end
    end

    % Add a new block of free memory
    if(neg_pos+10>neg_free), neg_free=neg_free+10000; neg_list((neg_pos+1):neg_free,:)=0; end
    
    % Add pixel with intensity nearest to the mean of the region, to the region
    dist = abs(neg_list(1:neg_pos,3)-reg_mean);
    [pixdist, index] = min(dist);
    J(x,y)=2; reg_size=reg_size+1;
    
    % Calculate the new mean of the region
    reg_mean= (reg_mean*reg_size + neg_list(index,3))/(reg_size+1);
    
    % Save the x and y coordinates of the pixel (for the neighbour add proccess)
    x = neg_list(index,1); y = neg_list(index,2);
    
    % Remove the pixel from the neighbour (check) list
    neg_list(index,:)=neg_list(neg_pos,:); neg_pos=neg_pos-1;
end

% Return the segmented area as logical matrix
J=J>1;
end